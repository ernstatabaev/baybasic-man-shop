package com.example.springsecurity.constant;

public class ErrorCodes {
    public static final String BAD_REQUEST = "SERVICE_CORE_BAD_REQUEST";
    public static final String NOT_FOUND = "SERVICE_CORE_NOT_FOUND";
    public static final String INTERNAL_SERVER_ERROR = "SERVICE_CORE_INTERNAL_SERVER_ERROR";
    public static final String INCORRECT_TIME_PERIOD_FORMAT = "SERVICE_CORE_INCORRECT_TIME_PERIOD_FORMAT";
    public static final String CUSTOMER_NOT_FOUND = "SERVICE_CORE_CUSTOMER_NOT_FOUND";
    public static final String CATEGORY_NOT_FOUND = "SERVICE_CORE_CATEGORY_NOT_FOUND";
    public static final String MERCHANT_NOT_FOUND = "SERVICE_CORE_MERCHANT_NOT_FOUND";
    public static final String ORDER_NOT_FOUND = "SERVICE_CORE_ORDER_NOT_FOUND";
    public static final String PLACE_NOT_FOUND = "SERVICE_CORE_PLACE_NOT_FOUND";
    public static final String PLACE_CUSTOMER_NOT_FOUND = "SERVICE_CORE_PLACE_CUSTOMER_NOT_FOUND";
    public static final String PLACE_ALREADY_EXIST = "SERVICE_CORE_PLACE_ALREADY_EXIST";
    public static final String PLACE_IMAGES_REQUIRED = "SERVICE_CORE_PLACE_IMAGES_REQUIRED";
    public static final String PLACE_ADDRESS_REQUIRED = "SERVICE_CORE_PLACE_ADDRESS_REQUIRED";
    public static final String PLACE_ALREADY_ACTIVATED = "SERVICE_CORE_PLACE_ALREADY_ACTIVATED";
    public static final String PLACE_ALREADY_DEACTIVATED = "SERVICE_CORE_PLACE_ALREADY_DEACTIVATED";
    public static final String PLACE_DEACTIVATED = "SERVICE_CORE_PLACE_WAS_DEACTIVATED";
    public static final String SERVICE_NOT_FOUND = "SERVICE_CORE_SERVICE_NOT_FOUND";
    public static final String SERVICE_TYPE_NOT_FOUND = "SERVICE_CORE_SERVICE_TYPE_NOT_FOUND";
    public static final String SELECTED_TIME_NOT_AVAILABLE = "SERVICE_CORE_SELECTED_TIME_NOT_AVAILABLE";
    public static final String BEGIN_TIME_LESS_CURRENT_TIME = "SERVICE_CORE_BEGIN_TIME_LESS_CURRENT_TIME";
    public static final String APPOINTMENT_TIMES_ARE_INTERSECT = "SERVICE_CORE_APPOINTMENT_TIMES_ARE_INTERSECT";
    public static final String INCORRECT_DATE = "SERVICE_CORE_INCORRECT_DATE";
    public static final String DATE_LESS_TODAY = "SERVICE_CORE_DATE_LESS_TODAY";
    public static final String APPOINTMENT_NOT_FOUND = "SERVICE_CORE_APPOINTMENT_NOT_FOUND";
    public static final String APPOINTMENT_SERVICE_NOT_FOUND = "SERVICE_CORE_APPOINTMENT_SERVICE_NOT_FOUND";
    public static final String NO_SERVICE_ITEM_SPECIALISTS = "SERVICE_CORE_NO_SERVICE_ITEM_SPECIALISTS";
    public static final String PHONE_NUMBERS_ARE_REQUIRED = "SERVICE_CORE_PHONE_NUMBERS_ARE_REQUIRED";
    public static final String WORKING_TIME_IS_NULL = "SERVICE_CORE_WORKING_TIME_IS_NULL";
    public static final String OPERATION_NOT_ALLOWED = "SERVICE_CORE_OPERATION_NOT_ALLOWED";
    public static final String SPECIALIST_HAS_NO_WORKING_PERIOD = "SERVICE_CORE_SPECIALIST_HAS_NO_WORKING_PERIOD";
    public static final String NO_AVAILABLE_SPECIALIST = "SERVICE_CORE_NO_AVAILABLE_SPECIALIST";
    public static final String SPECIALIST_NOT_FOUND = "SERVICE_CORE_SPECIALIST_NOT_FOUND";
    public static final String SERVICE_NOT_BELONG_TO_PLACE = "SERVICE_CORE_SERVICE_NOT_BELONG_TO_PLACE";
    public static final String TIME_IS_BUSY = "SERVICE_CORE_TIME_IS_BUSY";
    public static final String APPOINTMENT_ALREADY_COMPLETED = "SERVICE_CORE_APPOINTMENT_ALREADY_COMPLETED";
    public static final String INVALID_KAFKA_MESSAGE = "SERVICE_CORE_INVALID_KAFKA_MESSAGE";
    public static final String PAYMENT_STATUS_NOT_FOUND = "SERVICE_CORE_PAYMENT_STATUS_NOT_FOUND";
    public static final String STATUS_NOT_FOUND = "SERVICE_CORE_STATUS_NOT_FOUND";
    public static final String JOB_POSITION_NOT_FOUND = "SERVICE_CORE_PLACE_JOB_POSITION_NOT_FOUND";
    public static final String JOB_POSITION_ALREADY_EXIST = "SERVICE_CORE_PLACE_JOB_POSITION_ALREADY_EXIST";
    public static final String INVALID_OWNER = "SERVICE_CORE_INVALID_OWNER";
    public static final String THREAD_WAS_INTERRUPTED = "THREAD_WAS_INTERRUPTED";
    public static final String SPECIALIST_HAS_APPOINTMENTS = "SERVICE_CORE_SPECIALIST_HAS_APPOINTMENTS";
    public static final String CUSTOMER_APPOINTMENT_NOT_FOUND = "SERVICE_CORE_CUSTOMER_APPOINTMENT_NOT_FOUND";
    public static final String APPOINTMENT_ALREADY_APPROVED = "SERVICE_CORE_APPOINTMENT_IS_APPROVED";
    public static final String SMS_NOT_FOUND = "SERVICE_CORE_SMS_NOT_FOUND";
    public static final String SMS_CODE_NOT_MATCH = "SERVICE_CORE_SMS_CODE_NOT_MATCH";
    public static final String SMS_EXPIRED = "SERVICE_CORE_SMS_EXPIRED";
    public static final String NO_ACTIVE_SCHEDULE = "SERVICE_CORE_NO_ACTIVE_SCHEDULE";
    public static final String SPECIALIST_PRICE_NOT_CORRECT = "SERVICE_CORE_SPECIALIST_PRICE_NOT_CORRECT";
    public static final String SERVICE_ITEM_NOT_EXIST = "SERVICE_CORE_SERVICE_ITEM_NOT_EXIST";
    public static final String PLACE_NOT_BELONG_TO_MERCHANT = "SERVICE_CORE_PLACE_NOT_BELONG_TO_MERCHANT";
    public static final String JOB_POSITION_NOT_BELONG_TO_PLACE = "SERVICE_CORE_JOB_POSITION_NOT_BELONG_TO_PLACE";
    public static final String CANNOT_DELETE_VLIFE_APPOINTMENT = "SERVICE_CORE_CANNOT_DELETE_VLIFE_APPOINTMENT";
    public static final String SERVICE_WITH_SUCH_NAME_EXISTS = "SERVICE_CORE_SERVICE_WITH_SUCH_NAME_EXISTS";
    public static final String INVALID_TIME_PERIOD = "SERVICE_CORE_INVALID_TIME_PERIOD";
    public static final String INVALID_TIME_FORMAT = "SERVICE_CORE_INVALID_TIME_FORMAT";
    public static final String TIME_PERIODS_OVERLAP = "SERVICE_CORE_TIME_PERIODS_OVERLAP";
    public static final String ACTIVE_APPOINTMENTS_EXISTS = "SERVICE_CORE_ACTIVE_APPOINTMENTS_EXISTS";
    public static final String SEVERAL_PLACES_FOUND = "SERVICE_CORE_SEVERAL_PLACES_FOUND";
    public static final String DIFFERENT_PLACES_FOUND = "SERVICE_CORE_DIFFERENT_PLACES_FOUND";
    public static final String JOB_POSITION_HAS_SPECIALIST = "SERVICE_CORE_JOB_POSITION_HAS_SPECIALIST";
    public static final String DAY_OFF_DAY = "SERVICE_CORE_DAY_OFF_DAY";
    public static final String SPECIALIST_TIME_PERIOD_EXCEEDS_PLACE = "SERVICE_CORE_SPECIALIST_TIME_PERIOD_EXCEEDS_PLACE_PERIOD";
    public static final String DATE_FROM_IS_AFTER_DATE_TO = "DATE_FROM_IS_AFTER_DATE_TO";
    public static final String APPOINTMENT_ALREADY_PAID = "SERVICE_CORE_APPOINTMENT_ALREADY_PAID";
    public static final String APPOINTMENT_ALREADY_RECEIVED = "SERVICE_CORE_APPOINTMENT_ALREADY_RECEIVED";
    public static final String APPOINTMENT_NOT_APPROVED = "SERVICE_CORE_APPOINTMENT_NOT_APPROVED";
    public static final String APPOINTMENT_ALREADY_CANCELLED = "SERVICE_CORE_APPOINTMENT_ALREADY_CANCELLED";
    public static final String APPOINTMENT_NOT_CANCELLABLE = "SERVICE_CORE_APPOINTMENT_NOT_CANCELLABLE";
    public static final String APPOINTMENT_NOT_BELONG_TO_PLACE = "SERVICE_CORE_APPOINTMENT_NOT_BELONG_TO_PLACE";
    public static final String APPOINTMENT_NOT_BELONG_TO_CUSTOMER = "SERVICE_CORE_APPOINTMENT_NOT_BELONG_TO_CUSTOMER";
    public static final String FORBIDDEN = "SERVICE_CORE_FORBIDDEN";
    public static final String DUPLICATE_PHONE_NUMBERS = "DUPLICATE_PHONE_NUMBERS";
    public static final String FUNCTION_NOT_SUPPORTED = "SRV.FUNCTION_NOT_SUPPORTED";
    public static final String NOT_ACCEPTABLE = "SERVICE_CORE_NOT_ACCEPTABLE";
    public static final String CONFLICT = "SERVICE_CORE_CONFLICT";
    public static final String DIFFERENT_SERVICES_FOUND = "SERVICE_CORE_DIFFERENT_SERVICES_FOUND";
}
