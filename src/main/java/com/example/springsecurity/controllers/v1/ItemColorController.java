package com.example.springsecurity.controllers.v1;

import com.example.springsecurity.model.dto.itemsColor.ItemsColorViewDto;
import com.example.springsecurity.facade.ItemsColorFacade;
import com.example.springsecurity.model.dto.itemsColor.CreateItemsColorDto;
import com.example.springsecurity.model.dto.itemsColor.UpdateItemsColorDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/items-color")
@RequiredArgsConstructor
public class ItemColorController {

    //facade
    private final ItemsColorFacade itemsColorFacade;

    @PostMapping("/create")
    public ResponseEntity<Void> create(@RequestBody CreateItemsColorDto createItemsColorDto) {
        itemsColorFacade.create(createItemsColorDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update")
    public ResponseEntity<Void> update(@RequestBody UpdateItemsColorDto updateItemsSizeDto) {
        itemsColorFacade.update(updateItemsSizeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        itemsColorFacade.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<ItemsColorViewDto>> getItemsColor() {
        return ResponseEntity.ok(itemsColorFacade.getAllItemsColor());
    }

}
