package com.example.springsecurity.controllers.v1;

import com.example.springsecurity.facade.ItemTypeFacade;
import com.example.springsecurity.model.dto.PageDTO;
import com.example.springsecurity.model.dto.itemType.CreateItemTypeDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeViewDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeRequestDto;
import com.example.springsecurity.model.dto.itemType.UpdateItemTypeDto;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/item-type")
@RequiredArgsConstructor
public class ItemTypeController {

    //service
    private final ItemTypeFacade itemTypeFacade;

    @PostMapping("/create")
    @ApiOperation("Метод для создания типа товара")
    public ResponseEntity<Void> create(@RequestBody CreateItemTypeDto createItemTypeDto) {
        itemTypeFacade.create(createItemTypeDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update")
    @ApiOperation("Метод для редактироваия типов товара")
    public ResponseEntity<ItemTypeViewDto> update(@RequestBody UpdateItemTypeDto updateItemTypeDto) {
        itemTypeFacade.update(updateItemTypeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Метод для удаления типа товара")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        itemTypeFacade.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/find")
    @ApiOperation("Метод для получния товаров по коду")
    public PageDTO<ItemTypeViewDto> find(ItemTypeRequestDto itemTypeRequestDto, Pageable pageable) {
        return itemTypeFacade.find(itemTypeRequestDto, pageable);
    }

}
