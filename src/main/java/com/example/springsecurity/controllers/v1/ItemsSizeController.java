package com.example.springsecurity.controllers.v1;

import com.example.springsecurity.facade.ItemSizeFacade;
import com.example.springsecurity.model.dto.itemsSize.CreateItemsSizeDto;
import com.example.springsecurity.model.dto.itemsSize.ItemsSizeViewDto;
import com.example.springsecurity.model.dto.itemsSize.UpdateItemsSizeDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/items-size")
@RequiredArgsConstructor
public class ItemsSizeController {

    //facade
    private final ItemSizeFacade itemSizeFacade;

    @PostMapping("/create")
    public ResponseEntity<Void> create(@RequestBody CreateItemsSizeDto createItemsSizeDto) {
        itemSizeFacade.create(createItemsSizeDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/update")
    public ResponseEntity<Void> update(@RequestBody UpdateItemsSizeDto updateItemsSizeDto) {
        itemSizeFacade.update(updateItemsSizeDto);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        itemSizeFacade.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    public ResponseEntity<List<ItemsSizeViewDto>> getItemsSize() {
        return ResponseEntity.ok(itemSizeFacade.getAllItemsSize());
    }

}
