package com.example.springsecurity.converter.auth;

import com.example.springsecurity.model.User;
import com.example.springsecurity.model.dto.SignUpDto;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.nio.CharBuffer;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class SignUpDtoConverter implements Converter<SignUpDto, User> {

    private final PasswordEncoder passwordEncoder;

    @Override
    public User convert(SignUpDto source) {
        Date date = new Date();
        User target = new User();
        target.setFirstName(source.getFirstName());
        target.setLastName(source.getLastName());
        target.setLogin(source.getLogin());
        target.setPassword(passwordEncoder.encode(CharBuffer.wrap(source.getPassword())));
        target.setCreateDate(date);
        return target;
    }
}
