package com.example.springsecurity.converter.itemTypeConverter;

import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.model.dto.itemType.CreateItemTypeDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CreateItemTypeDtoConverter implements Converter<CreateItemTypeDto, ItemType> {

    @Override
    public ItemType convert(CreateItemTypeDto source) {
        Date date = new Date();
        ItemType target = new ItemType();
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
        target.setCreatedDate(date);
        return target;
    }
}
