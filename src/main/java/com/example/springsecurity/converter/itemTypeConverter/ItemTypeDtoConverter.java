package com.example.springsecurity.converter.itemTypeConverter;

import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.model.dto.itemType.ItemTypeViewDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class ItemTypeDtoConverter extends AbstractConverter<ItemType, ItemTypeViewDto> {

    @Override
    public void fill(ItemType source, ItemTypeViewDto target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
    }
}
