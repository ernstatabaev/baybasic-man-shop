package com.example.springsecurity.converter.itemTypeConverter;

import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.model.dto.itemType.UpdateItemTypeDto;
import com.example.springsecurity.utils.AbstractConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UpdateItemTypeDtoConverter extends AbstractConverter<UpdateItemTypeDto, ItemType> {

    @Override
    public void fill(UpdateItemTypeDto source, ItemType target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
    }
}
