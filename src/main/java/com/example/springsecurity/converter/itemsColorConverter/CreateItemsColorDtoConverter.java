package com.example.springsecurity.converter.itemsColorConverter;

import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.model.dto.itemsColor.CreateItemsColorDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CreateItemsColorDtoConverter extends AbstractConverter<CreateItemsColorDto, ItemsColor> {

    @Override
    public void fill(CreateItemsColorDto source, ItemsColor target) {
        Date date = new Date();
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
        target.setCreatedDate(date);
    }
}
