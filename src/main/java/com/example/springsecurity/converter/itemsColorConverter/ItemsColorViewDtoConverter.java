package com.example.springsecurity.converter.itemsColorConverter;

import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.model.dto.itemsColor.ItemsColorViewDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class ItemsColorViewDtoConverter extends AbstractConverter<ItemsColor, ItemsColorViewDto> {

    @Override
    public void fill(ItemsColor source, ItemsColorViewDto target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
    }
}
