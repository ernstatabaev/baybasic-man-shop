package com.example.springsecurity.converter.itemsColorConverter;

import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.model.dto.itemsColor.UpdateItemsColorDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class UpdateItemsColorDtoConverter extends AbstractConverter<UpdateItemsColorDto, ItemsColor> {

    @Override
    public void fill(UpdateItemsColorDto source, ItemsColor target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setIsActive(source.getIsActive());
    }
}
