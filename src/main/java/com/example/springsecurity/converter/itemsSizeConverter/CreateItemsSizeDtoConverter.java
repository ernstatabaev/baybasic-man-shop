package com.example.springsecurity.converter.itemsSizeConverter;

import com.example.springsecurity.model.ItemsSize;
import com.example.springsecurity.model.dto.itemsSize.CreateItemsSizeDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CreateItemsSizeDtoConverter extends AbstractConverter<CreateItemsSizeDto, ItemsSize> {

    @Override
    public void fill(CreateItemsSizeDto source, ItemsSize target) {
        Date date = new Date();
        target.setCode(source.getCode());
        target.setName(source.getName());
        target.setCreatedDate(date);
    }
}
