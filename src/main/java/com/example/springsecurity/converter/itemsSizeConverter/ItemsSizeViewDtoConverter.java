package com.example.springsecurity.converter.itemsSizeConverter;

import com.example.springsecurity.model.ItemsSize;
import com.example.springsecurity.model.dto.itemsSize.ItemsSizeViewDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class ItemsSizeViewDtoConverter extends AbstractConverter<ItemsSize, ItemsSizeViewDto> {

    @Override
    public void fill(ItemsSize source, ItemsSizeViewDto target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
    }
}
