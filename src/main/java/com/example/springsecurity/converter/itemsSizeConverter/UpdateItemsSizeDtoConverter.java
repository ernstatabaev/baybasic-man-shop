package com.example.springsecurity.converter.itemsSizeConverter;

import com.example.springsecurity.model.ItemsSize;
import com.example.springsecurity.model.dto.itemsSize.UpdateItemsSizeDto;
import com.example.springsecurity.utils.AbstractConverter;
import org.springframework.stereotype.Component;

@Component
public class UpdateItemsSizeDtoConverter extends AbstractConverter<UpdateItemsSizeDto, ItemsSize> {

    @Override
    public void fill(UpdateItemsSizeDto source, ItemsSize target) {
        target.setId(source.getId());
        target.setCode(source.getCode());
        target.setName(source.getName());
    }
}
