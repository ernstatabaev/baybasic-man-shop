package com.example.springsecurity.facade;

import com.example.springsecurity.model.dto.itemsSize.CreateItemsSizeDto;
import com.example.springsecurity.model.dto.itemsSize.ItemsSizeViewDto;
import com.example.springsecurity.model.dto.itemsSize.UpdateItemsSizeDto;

import java.util.List;

public interface ItemSizeFacade {

    void create(CreateItemsSizeDto itemsSizeDto);

    void update(UpdateItemsSizeDto updateItemsSizeDto);

    void delete(Long id);

    List<ItemsSizeViewDto> getAllItemsSize();

}
