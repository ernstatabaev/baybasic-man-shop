package com.example.springsecurity.facade;

import com.example.springsecurity.model.dto.PageDTO;
import com.example.springsecurity.model.dto.itemType.CreateItemTypeDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeViewDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeRequestDto;
import com.example.springsecurity.model.dto.itemType.UpdateItemTypeDto;
import org.springframework.data.domain.Pageable;

public interface ItemTypeFacade {

    void create(CreateItemTypeDto createItemTypeDto);

    void update(UpdateItemTypeDto updateItemTypeDto);

    PageDTO<ItemTypeViewDto> find(ItemTypeRequestDto itemTypeRequestDto, Pageable pageable);

    void delete(Long id);

}
