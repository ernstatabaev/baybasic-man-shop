package com.example.springsecurity.facade;

import com.example.springsecurity.model.dto.itemsColor.ItemsColorViewDto;
import com.example.springsecurity.model.dto.itemsColor.CreateItemsColorDto;
import com.example.springsecurity.model.dto.itemsColor.UpdateItemsColorDto;

import java.util.List;

public interface ItemsColorFacade {

    void create(CreateItemsColorDto itemsColorDto);

    void update(UpdateItemsColorDto updateItemsColorDto);

    void delete(Long id);

    List<ItemsColorViewDto> getAllItemsColor();

}
