package com.example.springsecurity.facade.impl;

import com.example.springsecurity.converter.itemsColorConverter.CreateItemsColorDtoConverter;
import com.example.springsecurity.converter.itemsColorConverter.ItemsColorViewDtoConverter;
import com.example.springsecurity.model.dto.itemsColor.ItemsColorViewDto;
import com.example.springsecurity.converter.itemsColorConverter.UpdateItemsColorDtoConverter;
import com.example.springsecurity.facade.ItemsColorFacade;
import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.model.dto.itemsColor.CreateItemsColorDto;
import com.example.springsecurity.model.dto.itemsColor.UpdateItemsColorDto;
import com.example.springsecurity.repository.ItemsColorRepository;
import com.example.springsecurity.service.ItemsColorService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.ValidateUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemColorFacadeImpl implements ItemsColorFacade {

    //service
    ItemsColorService itemsColorService;

    //converter
    CreateItemsColorDtoConverter createItemsColorDtoConverter;
    UpdateItemsColorDtoConverter updateItemsColorDtoConverter;
    ItemsColorViewDtoConverter itemsColorViewDtoConverter;

    //repository
    ItemsColorRepository itemsColorRepository;

    @Override
    public void create(CreateItemsColorDto itemsColorDto) {
        if (itemsColorService.isExistsByCode(itemsColorDto.getCode())) {
            throw AppExceptionUtils.badRequest("Такой тип уже существует" + itemsColorDto.getCode());
        }
        if (ValidateUtils.validateCode(itemsColorDto.getCode())) {
            throw AppExceptionUtils.badRequest("Некорректный код типа");
        }
        ItemsColor itemsColor = createItemsColorDtoConverter.convert(itemsColorDto);
        if (Objects.nonNull(itemsColor)) {
            itemsColorService.save(itemsColor);
        }
    }

    @Override
    public void update(UpdateItemsColorDto updateItemsColorDto) {
        ItemsColor itemsColor = itemsColorService.getById(updateItemsColorDto.getId());
        updateItemsColorDtoConverter.fill(updateItemsColorDto, itemsColor);
        itemsColorService.save(itemsColor);
    }

    @Override
    public void delete(Long id) {
        ItemsColor byId = itemsColorService.getById(id);
        itemsColorService.delete(byId.getId());
    }

    @Override
    public List<ItemsColorViewDto> getAllItemsColor() {
        List<ItemsColor> byOrderByCreatedDateDesc = itemsColorRepository.findByOrderByCreatedDateDesc();
        return itemsColorViewDtoConverter.convert(byOrderByCreatedDateDesc);
    }
}
