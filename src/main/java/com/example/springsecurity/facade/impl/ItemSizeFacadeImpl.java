package com.example.springsecurity.facade.impl;

import com.example.springsecurity.converter.itemsSizeConverter.CreateItemsSizeDtoConverter;
import com.example.springsecurity.converter.itemsSizeConverter.ItemsSizeViewDtoConverter;
import com.example.springsecurity.converter.itemsSizeConverter.UpdateItemsSizeDtoConverter;
import com.example.springsecurity.facade.ItemSizeFacade;
import com.example.springsecurity.model.ItemsSize;
import com.example.springsecurity.model.dto.itemsSize.CreateItemsSizeDto;
import com.example.springsecurity.model.dto.itemsSize.ItemsSizeViewDto;
import com.example.springsecurity.model.dto.itemsSize.UpdateItemsSizeDto;
import com.example.springsecurity.repository.ItemSizeRepository;
import com.example.springsecurity.service.ItemSizeService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.ValidateUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemSizeFacadeImpl implements ItemSizeFacade {

    //service
    ItemSizeService itemSizeService;

    //converter
    CreateItemsSizeDtoConverter createItemsSizeDtoConverter;
    UpdateItemsSizeDtoConverter updateItemsSizeDtoConverter;
    ItemsSizeViewDtoConverter itemsSizeViewDtoConverter;

    //repository
    ItemSizeRepository itemSizeRepository;

    @Override
    @Transactional
    public void create(CreateItemsSizeDto itemsSizeDto) {
        if (itemSizeService.isExistsByCode(itemsSizeDto.getCode())) {
            throw AppExceptionUtils.badRequest("Размер с таким кодом уже существует" + itemsSizeDto.getCode());
        }
        if (ValidateUtils.validateCode(itemsSizeDto.getCode())) {
            throw AppExceptionUtils.badRequest("Некорректный код размер");
        }
        ItemsSize itemsSize = createItemsSizeDtoConverter.convert(itemsSizeDto);
        if (Objects.nonNull(itemsSize)) {
            itemSizeService.save(itemsSize);
        }
    }

    @Override
    @Transactional
    public void update(UpdateItemsSizeDto updateItemsSizeDto) {
        if (Objects.isNull(updateItemsSizeDto.getId())) {
            throw AppExceptionUtils.notFound("Обновить размер товара без id невозможно");
        }
        ItemsSize itemsSize = itemSizeService.getById(updateItemsSizeDto.getId());
        updateItemsSizeDtoConverter.fill(updateItemsSizeDto, itemsSize);
        itemSizeRepository.save(itemsSize);
    }

    @Override
    public void delete(Long id) {
        ItemsSize itemsSize = itemSizeService.getById(id);
        itemSizeService.delete(itemsSize.getId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<ItemsSizeViewDto> getAllItemsSize() {
        List<ItemsSize> itemSize = itemSizeRepository.findByOrderByCreatedDateDesc();
        return itemsSizeViewDtoConverter.convert(itemSize);
    }
}
