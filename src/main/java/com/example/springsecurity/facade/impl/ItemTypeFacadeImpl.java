package com.example.springsecurity.facade.impl;

import com.example.springsecurity.converter.itemTypeConverter.CreateItemTypeDtoConverter;
import com.example.springsecurity.converter.itemTypeConverter.UpdateItemTypeDtoConverter;
import com.example.springsecurity.facade.ItemTypeFacade;
import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.model.dto.PageDTO;
import com.example.springsecurity.model.dto.itemType.CreateItemTypeDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeViewDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeRequestDto;
import com.example.springsecurity.model.dto.itemType.UpdateItemTypeDto;
import com.example.springsecurity.repository.JPQL.ItemTypeCriteria;
import com.example.springsecurity.service.ItemTypeService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.ValidateUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemTypeFacadeImpl implements ItemTypeFacade {

    //converter
    UpdateItemTypeDtoConverter updateItemTypeDtoConverter;
    CreateItemTypeDtoConverter createItemTypeDtoConverter;

    //service
    ItemTypeService itemTypeService;

    //criteria
    ItemTypeCriteria itemTypeCriteria;

    @Override
    public void create(CreateItemTypeDto createItemTypeDto) {
        if (itemTypeService.isExistsByCode(createItemTypeDto.getCode())) {
            throw AppExceptionUtils.badRequest("Такой тип уже существует" + createItemTypeDto.getCode());
        }
        if (ValidateUtils.validateCode(createItemTypeDto.getCode())) {
            throw AppExceptionUtils.badRequest("Некорректный код типа");
        }
        ItemType itemType = createItemTypeDtoConverter.convert(createItemTypeDto);
        if (Objects.nonNull(itemType)) {
            itemTypeService.save(itemType);
        }
    }

    @Override
    @Transactional
    public void update(UpdateItemTypeDto updateItemTypeDto) {
        ItemType itemType = itemTypeService.getById(updateItemTypeDto.getId());
        updateItemTypeDtoConverter.fill(updateItemTypeDto, itemType);
        itemTypeService.save(itemType);
    }

    @Override
    public PageDTO<ItemTypeViewDto> find(ItemTypeRequestDto itemTypeRequestDto, Pageable pageable) {
        return itemTypeCriteria.find(itemTypeRequestDto, pageable);
    }

    @Override
    public void delete(Long id) {
        ItemType itemType = itemTypeService.getById(id);
        itemTypeService.delete(itemType.getId());
    }

}
