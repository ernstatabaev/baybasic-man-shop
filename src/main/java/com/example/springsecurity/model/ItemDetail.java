package com.example.springsecurity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "item_detail")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ItemDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "items_table_id")
    Items items;

    @Column(name = "compound")
    String compound;

    @Column(name = "brand")
    String brand;

    @Column(name = "country_of_origin")
    String countryOfOrigin;

    @Column(name = "season")
    String season;

    @Column(name = "item_length")
    String iteLength;

    @Column(name = "length_sleeve")
    String lengthSleeve;

    @Column(name = "model_height")
    String modelHeight;

    @Column(name = "created_date")
    Date createdDate;
}
