package com.example.springsecurity.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "items_table")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Items {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "item_type_id")
    ItemType itemType;

    @Column(name = "vendor_code")
    String vendorCode;

    @Column(name = "name")
    String name;

    @Column(name = "price")
    String price;

    @Column(name = "product_evaluation")
    String productEvaluation;

    @ManyToOne
    @JoinColumn(name = "item_colors_id")
    ItemsColor itemsColor;

    @ManyToOne
    @JoinColumn(name = "item_size_id")
    ItemsSize itemsSize;

    @OneToOne
    @JoinColumn(name = "item_detail_id")
    ItemDetail itemDetail;

    @Column(name = "created_date")
    Date createdDate;
}
