package com.example.springsecurity.model.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SignUpDto {

    @NotEmpty
    String firstName;

    @NotEmpty
    String lastName;

    @NotEmpty
    String login;

    @NotEmpty
    char[] password;

}
