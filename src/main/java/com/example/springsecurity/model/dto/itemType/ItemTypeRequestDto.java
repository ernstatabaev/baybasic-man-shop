package com.example.springsecurity.model.dto.itemType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("Модель для получения пагинированного списока типов товара")
public class ItemTypeRequestDto {

    @ApiModelProperty("Тип товара")
    String code;

    @ApiModelProperty("Пармаетр для поиска")
    String query;

}
