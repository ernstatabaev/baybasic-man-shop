package com.example.springsecurity.model.dto.itemType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("Общая модель типов товара")
public class ItemTypeViewDto {

    @ApiModelProperty("Идентификатор типа товара")
    Long id;
    @ApiModelProperty("Тип товара")
    String code;
    @ApiModelProperty("Локализованное название товара")
    Map<String, String> name;
    @ApiModelProperty("Признак активности типа")
    Boolean isActive;

}
