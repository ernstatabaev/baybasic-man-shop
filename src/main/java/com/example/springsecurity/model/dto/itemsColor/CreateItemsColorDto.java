package com.example.springsecurity.model.dto.itemsColor;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Date;
import java.util.Map;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("Модель для создания цветов товара")
public class CreateItemsColorDto {
    @ApiModelProperty("Код цвета товара")
    String code;
    @ApiModelProperty("Локализованное название цвета товара")
    Map<String, String> name;
    @ApiModelProperty("Признак активности цвета")
    Boolean isActive;
    @ApiModelProperty("Дата создания цвета")
    Date createdDate;
}
