package com.example.springsecurity.model.dto.itemsColor;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("Общая модель цыетов товара")
public class ItemsColorViewDto {
    @ApiModelProperty("Идентификатор цыета товара")
    Long id;
    @ApiModelProperty("Код цвета товара")
    String code;
    @ApiModelProperty("Локализованное название цвета товара")
    Map<String, String> name;
    @ApiModelProperty("Признак активности цвета")
    Boolean isActive;
}
