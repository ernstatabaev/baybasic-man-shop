package com.example.springsecurity.model.dto.itemsSize;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ItemsSizeViewDto {

    @ApiModelProperty("Идентификатор типа товара")
    Long id;
    @ApiModelProperty("Тип товара")
    String code;
    @ApiModelProperty("Локализованное название размера")
    Map<String, String> name;

}
