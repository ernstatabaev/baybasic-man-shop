package com.example.springsecurity.model.dto.itemsSize;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("Модель для редактирования размеров одежды")
public class UpdateItemsSizeDto {

    @ApiModelProperty("Идентификатор товара")
    Long id;
    @ApiModelProperty("Размер товара")
    String code;
    @ApiModelProperty("Локализованное название размера")
    Map<String, String> name;

}
