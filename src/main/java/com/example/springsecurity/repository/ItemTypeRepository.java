package com.example.springsecurity.repository;

import com.example.springsecurity.model.ItemType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemTypeRepository extends JpaRepository<ItemType, Long> {

    @Query("select new java.lang.Boolean(count(it) > 0) from ItemType it where it.code = :code")
    Boolean hasCode(@Param("code") String code);

}
