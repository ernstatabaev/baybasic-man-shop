package com.example.springsecurity.repository;

import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.model.ItemsSize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemsColorRepository extends JpaRepository<ItemsColor, Long> {

    @Query("select new java.lang.Boolean(count(its) > 0) from ItemsSize its where its.code = :code")
    boolean hasCode(@Param("code") String code);

    List<ItemsColor> findByOrderByCreatedDateDesc();

}
