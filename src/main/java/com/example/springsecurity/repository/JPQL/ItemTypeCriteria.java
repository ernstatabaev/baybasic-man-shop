package com.example.springsecurity.repository.JPQL;

import com.example.springsecurity.converter.itemTypeConverter.ItemTypeDtoConverter;
import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.model.QItemType;
import com.example.springsecurity.model.dto.PageDTO;
import com.example.springsecurity.model.dto.itemType.ItemTypeViewDto;
import com.example.springsecurity.model.dto.itemType.ItemTypeRequestDto;
import com.example.springsecurity.utils.PageUtils;
import com.example.springsecurity.utils.QueryDslExpressions;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Objects;

@Repository
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemTypeCriteria {

    //manager
    EntityManager entityManager;

    //converter
    ItemTypeDtoConverter itemTypeDtoConverter;

    public PageDTO<ItemTypeViewDto> find(ItemTypeRequestDto itemTypeRequestDto, Pageable pageable) {
        JPAQuery<ItemType> query = new JPAQuery<>(entityManager);
        query.from(QItemType.itemType);

        BooleanBuilder builder = new BooleanBuilder();

        if (Objects.nonNull(itemTypeRequestDto.getCode())) {
            builder.and(QItemType.itemType.code.eq(itemTypeRequestDto.getCode()));
        }

        if (Objects.nonNull(itemTypeRequestDto.getQuery())) {
            String toLowerCase = itemTypeRequestDto.getQuery().toLowerCase();
            builder.and(QueryDslExpressions.localizedLike(QItemType.itemType.name, toLowerCase))
                    .or(QItemType.itemType.id.like(toLowerCase));
        }
        query.where(builder);
        query.orderBy(QItemType.itemType.createdDate.desc());
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());
        List<ItemType> fetch = query.fetch();
        List<ItemTypeViewDto> result = itemTypeDtoConverter.convert(fetch);
        return PageUtils.getPage(result, pageable, query::fetchCount);
    }

}
