package com.example.springsecurity.service;

import com.example.springsecurity.model.dto.CredentialsDto;
import com.example.springsecurity.model.dto.UserDto;

public interface AuthenticationService {

    UserDto authenticate(CredentialsDto credentialsDto);

    UserDto findByLogin(String login);

}
