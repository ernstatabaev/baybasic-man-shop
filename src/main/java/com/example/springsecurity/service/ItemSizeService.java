package com.example.springsecurity.service;

import com.example.springsecurity.model.ItemsSize;

import java.util.List;

public interface ItemSizeService {

    void save(ItemsSize itemsSize);

    ItemsSize getById(Long id);

    void delete(Long id);

    boolean isExistsByCode(String code);

    List<ItemsSize> findAll();
}
