package com.example.springsecurity.service;

import com.example.springsecurity.model.ItemType;

public interface ItemTypeService {

    void save(ItemType itemType);

    ItemType getById(Long id);

    void delete(Long id);

    boolean isExistsByCode(String code);

}
