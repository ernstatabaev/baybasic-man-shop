package com.example.springsecurity.service;

import com.example.springsecurity.model.ItemsColor;

public interface ItemsColorService {

    void save(ItemsColor itemsColor);

    ItemsColor getById(Long id);

    void delete(Long id);

    boolean isExistsByCode(String code);

}
