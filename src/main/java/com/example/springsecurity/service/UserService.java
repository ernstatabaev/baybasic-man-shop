package com.example.springsecurity.service;

import com.example.springsecurity.model.dto.SignUpDto;
import com.example.springsecurity.model.dto.UserDto;

public interface UserService {

    void signUp(SignUpDto user);

}
