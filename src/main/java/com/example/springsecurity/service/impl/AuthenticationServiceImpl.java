package com.example.springsecurity.service.impl;

import com.example.springsecurity.converter.auth.UserDtoConverter;
import com.example.springsecurity.model.User;
import com.example.springsecurity.model.dto.CredentialsDto;
import com.example.springsecurity.model.dto.UserDto;
import com.example.springsecurity.repository.UserRepository;
import com.example.springsecurity.service.AuthenticationService;
import com.example.springsecurity.utils.AppExceptionUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.CharBuffer;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    //repository
    private final UserRepository userRepository;

    //Password encoder
    private final PasswordEncoder passwordEncoder;

    //converter
    private final UserDtoConverter userDtoConverter;

    @Transactional
    public UserDto authenticate(CredentialsDto credentialsDto) {
        User user = userRepository.findByLogin(credentialsDto.getLogin())
                .orElseThrow(() -> AppExceptionUtils.notFound("User not found", HttpStatus.NOT_FOUND));
        if (passwordEncoder.matches(CharBuffer.wrap(credentialsDto.getPassword()), user.getPassword())) {
            log.debug("User {} authenticated correctly", credentialsDto.getLogin());
            return userDtoConverter.convert(user);
        }
        throw AppExceptionUtils.badRequest("Invalid password", HttpStatus.BAD_REQUEST);
    }

    public UserDto findByLogin(String login) {
        User user = userRepository.findByLogin(login).orElseThrow(() -> AppExceptionUtils.notFound("Login not found", HttpStatus.NOT_FOUND));
        return userDtoConverter.convert(user);
    }
}
