package com.example.springsecurity.service.impl;

import com.example.springsecurity.model.ItemType;
import com.example.springsecurity.repository.ItemTypeRepository;
import com.example.springsecurity.service.ItemTypeService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.LanguageUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemTypeServiceImpl implements ItemTypeService {

    //repository
    ItemTypeRepository itemTypeRepository;

    @Override
    public void save(ItemType itemType) {
        if (StringUtils.isBlank(itemType.getCode())) {
            throw AppExceptionUtils.badRequest("Код типа не может быть пустой!");
        }
        itemType.setCode(itemType.getCode().toUpperCase());
        LanguageUtils.checkLatinEnum(itemType.getCode());
        itemTypeRepository.save(itemType);
        log.info("Item successfully saved");
    }

    @Override
    public boolean isExistsByCode(String code) {
        return itemTypeRepository.hasCode(code);
    }


    @Override
    public void delete(Long id) {
        ItemType byId = getById(id);
        itemTypeRepository.delete(byId);
    }

    @Override
    public ItemType getById(Long id) {
        return itemTypeRepository.findById(id)
                .orElseThrow(() -> AppExceptionUtils.notFound("ItemType с ID: %s не найден", id));
    }
}
