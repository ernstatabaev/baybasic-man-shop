package com.example.springsecurity.service.impl;

import com.example.springsecurity.model.ItemsColor;
import com.example.springsecurity.repository.ItemsColorRepository;
import com.example.springsecurity.service.ItemsColorService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.LanguageUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemsColorServiceImpl implements ItemsColorService {

    //repository
    ItemsColorRepository itemsColorRepository;

    @Override
    public void save(ItemsColor itemsColor) {
        if (StringUtils.isBlank(itemsColor.getCode())) {
            throw AppExceptionUtils.badRequest("Код типа не может быть пустой!");
        }
        itemsColor.setCode(itemsColor.getCode().toUpperCase());
        LanguageUtils.checkLatinEnum(itemsColor.getCode());
        itemsColorRepository.save(itemsColor);
        log.info("Color successfully saved");
    }

    @Override
    public ItemsColor getById(Long id) {
        return itemsColorRepository.findById(id)
                .orElseThrow(() -> AppExceptionUtils.notFound("ItemType с ID: %s не найден", id));
    }

    @Override
    public void delete(Long id) {
        ItemsColor byId = getById(id);
        itemsColorRepository.delete(byId);
    }

    @Override
    public boolean isExistsByCode(String code) {
        return itemsColorRepository.hasCode(code);
    }
}
