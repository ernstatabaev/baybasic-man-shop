package com.example.springsecurity.service.impl;

import com.example.springsecurity.model.ItemsSize;
import com.example.springsecurity.repository.ItemSizeRepository;
import com.example.springsecurity.service.ItemSizeService;
import com.example.springsecurity.utils.AppExceptionUtils;
import com.example.springsecurity.utils.LanguageUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class ItemsSizeImpl implements ItemSizeService {

    //repository
    ItemSizeRepository itemSizeRepository;

    @Override
    public void save(ItemsSize itemsSize) {
        if (StringUtils.isBlank(itemsSize.getCode())) {
            throw AppExceptionUtils.badRequest("Код размера не может быть пустым!");
        }
        itemsSize.setCode(itemsSize.getCode().toUpperCase());
        LanguageUtils.checkLatinEnum(itemsSize.getCode());
        itemSizeRepository.save(itemsSize);
    }

    @Override
    public ItemsSize getById(Long id) {
        return itemSizeRepository.findById(id)
                .orElseThrow(() -> AppExceptionUtils.notFound("ItemSize с ID: %s не найден", id));
    }

    @Override
    public void delete(Long id) {
        ItemsSize itemsSize = getById(id);
        itemSizeRepository.delete(itemsSize);
    }

    @Override
    public boolean isExistsByCode(String code) {
        return itemSizeRepository.hasCode(code);
    }

    @Override
    public List<ItemsSize> findAll() {
        return itemSizeRepository.findAll();
    }

}
