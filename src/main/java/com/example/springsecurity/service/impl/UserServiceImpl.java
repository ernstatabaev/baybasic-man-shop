package com.example.springsecurity.service.impl;

import com.example.springsecurity.converter.auth.SignUpDtoConverter;
import com.example.springsecurity.model.User;
import com.example.springsecurity.model.dto.SignUpDto;
import com.example.springsecurity.repository.UserRepository;
import com.example.springsecurity.service.UserService;
import com.example.springsecurity.utils.AppExceptionUtils;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UserServiceImpl implements UserService {

    //repository
    UserRepository userRepository;

    //converter
    SignUpDtoConverter signUpDtoConverter;

    @Override
    public void signUp(SignUpDto user) {
        Optional<User> optionalUser = userRepository.findByLogin(user.getLogin());
        if (optionalUser.isPresent()) {
            throw AppExceptionUtils.badRequest("Login already exists", HttpStatus.BAD_REQUEST);
        }
        User convert = signUpDtoConverter.convert(user);
        if (Objects.nonNull(convert)) {
            userRepository.save(convert);
        }
        log.info("Creating new user {}", user.getLogin());
    }
}
