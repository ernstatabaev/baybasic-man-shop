package com.example.springsecurity.utils;

import com.example.springsecurity.constant.ErrorCodes;
import com.example.springsecurity.exception.AppException;
import lombok.experimental.UtilityClass;
import org.springframework.http.HttpStatus;

@UtilityClass
public class AppExceptionUtils {
    public AppException badRequest(String message) {
        return new AppException(HttpStatus.BAD_REQUEST, ErrorCodes.BAD_REQUEST, message);
    }

    public AppException badRequest(String message, String code) {
        return new AppException(HttpStatus.BAD_REQUEST, code, message);
    }

    public AppException badRequest(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.BAD_REQUEST, message);
    }

    public AppException badRequest(String message, Object... params) {
        return new AppException(HttpStatus.BAD_REQUEST, ErrorCodes.BAD_REQUEST, String.format(message, params));
    }

    public AppException notFound(String message) {
        return new AppException(HttpStatus.NOT_FOUND, ErrorCodes.NOT_FOUND, message);
    }

    public AppException notFound(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.NOT_FOUND, message);
    }

    public AppException notFound(String message, String code) {
        return new AppException(HttpStatus.NOT_FOUND, code, message);
    }

    public AppException notFound(String message, Object... params) {
        return new AppException(HttpStatus.NOT_FOUND, ErrorCodes.NOT_FOUND, String.format(message, params));
    }

    public AppException internalServerError(String message) {
        return new AppException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorCodes.INTERNAL_SERVER_ERROR, message);
    }

    public AppException internalServerError(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.INTERNAL_SERVER_ERROR, message);
    }

    public AppException internalServerError(String message, String code) {
        return new AppException(HttpStatus.INTERNAL_SERVER_ERROR, code, message);
    }

    public AppException internalServerError(String message, Object... params) {
        return new AppException(HttpStatus.INTERNAL_SERVER_ERROR, ErrorCodes.INTERNAL_SERVER_ERROR, String.format(message, params));
    }

    public AppException forbidden(String message) {
        return new AppException(HttpStatus.FORBIDDEN, ErrorCodes.FORBIDDEN, message);
    }

    public AppException forbidden(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.FORBIDDEN, message);
    }

    public AppException forbidden(String message, String code) {
        return new AppException(HttpStatus.FORBIDDEN, code, message);
    }

    public AppException forbidden(String message, Object... params) {
        return new AppException(HttpStatus.FORBIDDEN, ErrorCodes.FORBIDDEN, String.format(message, params));
    }

    public AppException notAcceptable(String message) {
        return new AppException(HttpStatus.NOT_ACCEPTABLE, ErrorCodes.NOT_ACCEPTABLE, message);
    }

    public AppException notAcceptable(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.NOT_ACCEPTABLE, message);
    }

    public AppException notAcceptable(String message, String code) {
        return new AppException(HttpStatus.NOT_ACCEPTABLE, code, message);
    }

    public AppException notAcceptable(String message, Object... params) {
        return new AppException(HttpStatus.NOT_ACCEPTABLE, ErrorCodes.NOT_ACCEPTABLE, String.format(message, params));
    }

    public AppException conflict (String message) {
        return new AppException(HttpStatus.CONFLICT, ErrorCodes.CONFLICT, message);
    }

    public AppException conflict(String message, HttpStatus status) {
        return new AppException(status, ErrorCodes.CONFLICT, message);
    }

    public AppException conflict(String message, String code) {
        return new AppException(HttpStatus.CONFLICT, code, message);
    }

    public AppException conflict(String message, Object... params) {
        return new AppException(HttpStatus.CONFLICT, ErrorCodes.CONFLICT, String.format(message, params));
    }
}
