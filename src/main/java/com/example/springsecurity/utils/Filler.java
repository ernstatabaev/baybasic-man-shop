package com.example.springsecurity.utils;

public interface Filler<S, T> {
    void fill(S var1, T var2);

    T createTarget();
}
