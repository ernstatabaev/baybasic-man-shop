package com.example.springsecurity.utils;

import com.querydsl.core.util.ArrayUtils;
import lombok.experimental.UtilityClass;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.DayOfWeek;
import java.time.format.TextStyle;
import java.util.*;
import java.util.regex.Pattern;

@UtilityClass
public class LanguageUtils {

    public static final String EN_LANG = "en";
    public static final String KK_LANG = "kk";
    public static final String RU_LANG = "ru";
    public static final String ZH_LANG = "zh";
    private static final String[] LANGUAGES = {EN_LANG, KK_LANG, RU_LANG, ZH_LANG};
    private static final Set<String> LANGUAGES_SET = Set.of(LANGUAGES);
    private static final String DEFAULT_LANGUAGE = RU_LANG;
    private static final String NULLABLE_TEXT = "[text_not_found]";

    private static final String LATIN_ALPHABET_AS_ENUM = "[^a-zA-Z_]";
    private static final String LATIN_ALPHABET_AS_NOT_ENUM = "[\\\\^a-zA-Z_]";

    private static final Pattern LATIN_ALPHABET_AS_ENUM_PATTERN = Pattern.compile(LATIN_ALPHABET_AS_ENUM);


    /**
     * Проверяет что слово содержит только латинские буквы.
     * А так же что все буквы заглавные.
     * Т.е Проверят что слово является ENUM-ом
     *
     * @param words слова
     */
    public static void checkLatinEnum(String... words) {
        if (ArrayUtils.isEmpty(words)) {
            // Если массив пустой то false
            throw new IllegalStateException("Слова не могут буть пустыми");
        }
        for (String word : words) {
            if (LATIN_ALPHABET_AS_ENUM_PATTERN.matcher(word).replaceAll("").length() < word.length()) {
                throw new IllegalStateException(String.format("Слово содержит запретные символы такие как эти: %s",
                        StringUtils.join(RegExUtils.replacePattern(word, LATIN_ALPHABET_AS_NOT_ENUM, "").split(""), ", "))
                );
            }
        }
    }

    public static String getCurrentLanguage() {
        Optional<HttpServletRequest> request = getCurrentHttpRequest();
        if (request.isPresent()) {
            return getCurrentLanguage(request.get());
        }
        return DEFAULT_LANGUAGE;
    }

    public static String getLocalizedName(DayOfWeek dayOfWeek) {
        return dayOfWeek
                .getDisplayName(TextStyle.SHORT, new Locale(LanguageUtils.getCurrentLanguage()))
                .toUpperCase(Locale.ROOT);
    }

    public static String getCurrentLanguage(HttpServletRequest request) {
        Locale locale = request.getLocale();
        if (Objects.isNull(locale)) {
            return DEFAULT_LANGUAGE;
        } else {
            return locale.getLanguage();
        }
    }

    public static Optional<String> getLocalizedField(final Map<String, String> map) {
        String localizedFiled = MapUtils.getString(map, getCurrentLanguage());
        return Optional.ofNullable(localizedFiled);
    }

    public static String getNullable(final Map<String, String> map) {
        return getLocalizedField(map).orElse(NULLABLE_TEXT);
    }

    public static String getOrDefault(final Map<String, String> map) {
        return getLocalizedField(map).orElse(MapUtils.getString(map, DEFAULT_LANGUAGE));
    }

    private static Optional<HttpServletRequest> getCurrentHttpRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        if (requestAttributes instanceof ServletRequestAttributes) {
            return Optional.of(((ServletRequestAttributes) requestAttributes).getRequest());
        }
        return Optional.empty();
    }

    public static Map<String, String> initLocale(String defaultValue) {
        Map<String, String> map = new HashMap<>();
        for (String language : LANGUAGES) {
            map.put(language, defaultValue);
        }
        return map;
    }


    /**
     * Валидирует чо объект содержит все необходимые ключи локализации
     * @param map локалей
     */
    public static void validateLocale(Map<String, String> map) {
        if (MapUtils.isEmpty(map)) {
            throw AppExceptionUtils.badRequest("Отсутствуют языки");
        }

        Set<String> missingLanguages = new HashSet<>(LANGUAGES_SET);
        missingLanguages.removeAll(map.keySet());

        if (CollectionUtils.isNotEmpty(missingLanguages)) {
            throw AppExceptionUtils.badRequest("Отсутствуют языки %s", missingLanguages);
        }

        Set<String> excessLocales = new HashSet<>(map.keySet());
        excessLocales.removeAll(LANGUAGES_SET);

        if (CollectionUtils.isNotEmpty(excessLocales)) {
            throw AppExceptionUtils.badRequest("Отправлены лишние или не правильные следующие ключи языков %s", excessLocales);
        }
    }

    public static void cleanAndValidate(Map<String, String> map) {
        validateLocale(map);
        cleanMap(map);
    }

    public static void cleanMap(Map<String, String> map) {
        for (String locale : LANGUAGES_SET) {
            map.put(locale, map.get(locale).trim().replaceAll(" +", " "));
        }
    }

}
