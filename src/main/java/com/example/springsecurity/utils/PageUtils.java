package com.example.springsecurity.utils;

import com.example.springsecurity.model.dto.PageDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.util.Assert;

import java.util.List;
import java.util.function.LongSupplier;

public final class PageUtils {

    public static <T> PageDTO<T> getPage(List<T> content, Pageable pageable, LongSupplier totalSupplier) {
        Assert.notNull(content, "Content must not be null!");
        Assert.notNull(pageable, "Pageable must not be null!");
        Assert.notNull(totalSupplier, "TotalSupplier must not be null!");
        if (!pageable.isUnpaged() && pageable.getOffset() != 0L) {
            return content.size() != 0 && pageable.getPageSize() > content.size() ? of(pageable.getOffset() + (long)content.size(), content, pageable.getPageNumber(), pageable) : of(totalSupplier.getAsLong(), content, pageable.getPageNumber(), pageable);
        } else {
            return !pageable.isUnpaged() && pageable.getPageSize() <= content.size() ? of(totalSupplier.getAsLong(), content, pageable.getPageNumber(), pageable) : of((long)content.size(), content, pageable.getPageNumber(), pageable);
        }
    }

    private static <T> PageDTO<T> of(Long total, List<T> content, Integer pageNumber, Pageable pageable) {
        PageDTO<T> dto = new PageDTO();
        dto.setTotal(total);
        dto.setContent(content);
        dto.setPageNumber(pageNumber);
        dto.setTotalPages((int)Math.ceil((double)total / (double)pageable.getPageSize()));
        return dto;
    }

    private PageUtils() {
        throw new UnsupportedOperationException("This is a utility class and cannot be instantiated");
    }

}
