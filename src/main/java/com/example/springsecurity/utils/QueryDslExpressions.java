package com.example.springsecurity.utils;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringTemplate;
import lombok.experimental.UtilityClass;

@UtilityClass
public class QueryDslExpressions {

    public static <E extends Comparable<E>> StringTemplate jsonText(Path<?> path, String property) {
        return Expressions.stringTemplate("json_extract_path_text(to_json({0}), {1})", path, property);
    }

    public BooleanExpression localizedLike(Path<?> name, String text) {
        String lowerCase = text.toLowerCase();
        return jsonText(name, LanguageUtils.RU_LANG).lower().like("%" + lowerCase + "%")
                .or(jsonText(name, LanguageUtils.EN_LANG).lower().like("%" + lowerCase + "%"))
                .or(jsonText(name, LanguageUtils.KK_LANG).lower().like("%" + lowerCase + "%"))
                .or(jsonText(name, LanguageUtils.ZH_LANG).lower().like("%" + lowerCase + "%"));
    }

}
