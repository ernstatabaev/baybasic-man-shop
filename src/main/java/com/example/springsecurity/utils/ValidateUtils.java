package com.example.springsecurity.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidateUtils {

    public boolean validateCode(String code) {
        if (!(Character.isUpperCase(code.charAt(0)) && Character.isUpperCase(code.charAt(code.length() - 1)))) {
            return false;
        }
        for (int i = 0; i < code.length(); i++) {
            if (!(code.charAt(i) == '_' && code.charAt(i - 1) != '_' || Character.isUpperCase(code.charAt(i)))) {
                return false;
            }
        }
        return true;
    }

}
