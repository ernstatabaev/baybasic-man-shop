create table user_table (
    id                     bigserial not null
            constraint user_pkey
                primary key,
    first_name varchar(100) not null,
    last_name varchar(100) not null,
    login varchar(100) not null,
    password varchar(100) not null,
    create_date timestamp with time zone not null
);