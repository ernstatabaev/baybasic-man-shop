create table item_detail
(
    id                  bigserial not null primary key,
    compound            varchar(255),
    brand               varchar(255),
    country_of_origin   varchar(255),
    season              varchar(255),
    item_length         varchar(255),
    length_sleeve       varchar(255),
    model_height        varchar(255),
    created_date        timestamp with time zone not null default now()
);
comment on table item_detail is 'Таблица о деталях товара';
comment on column item_detail.id is 'Родительский ID';
comment on column item_detail.compound is 'Состав товара';
comment on column item_detail.brand is 'Бренд товара';
comment on column item_detail.country_of_origin is 'Страна производства';
comment on column item_detail.season is 'Сезон';
comment on column item_detail.item_length is 'Длина товара';
comment on column item_detail.length_sleeve is 'Длина рукава';
comment on column item_detail.model_height is 'Рост модели';
comment on column item_detail.created_date is 'Дата создания';

create table item_colors
(
    id              bigserial not null primary key,
    code            varchar(255) not null unique,
    name            jsonb not null,
    is_active       boolean default false not null,
    created_date    timestamp with time zone not null default now()
);
comment on table item_colors is 'Таблица о цветах товара';
comment on column item_colors.id is 'Родительский ID';
comment on column item_colors.code is 'Код это цвет пример -> "BLACK"';
comment on column item_colors.name is 'Локализованое название цвета';
comment on column item_colors.created_date is 'Дата создания';

create table item_size
(
    id              bigserial not null primary key,
    code            varchar(255) not null unique,
    name            jsonb not null,
    created_date    timestamp with time zone not null default now()
);
comment on table item_size is 'Таблица о цветах товара';
comment on column item_size.id is 'Родительский ID';
comment on column item_size.code is 'Код это размер пример -> "S,M,L"';
comment on column item_size.created_date is 'Дата создания';

create table item_type
(
    id              bigserial not null primary key,
    code            varchar(255) not null unique,
    name            jsonb not null,
    is_active       boolean default false not null,
    created_date    timestamp with time zone not null default now()
);
comment on table item_type is 'Таблица о типах товара';
comment on column item_type.id is 'Родительский ID';
comment on column item_type.code is 'Код типа товара "Футболка"';
comment on column item_type.name is 'Локализованое название типа товара';
comment on column item_type.is_active is 'Признак активности товара';
comment on column item_type.created_date is 'Дата создания';

create table items
(
    id                  bigserial not null primary key,
    item_type_id        bigint references item_type(id),
    vendor_code         varchar(255),
    name           varchar(255),
    price               varchar(255),
    product_evaluation  varchar(255),
    item_colors_id      bigint references item_colors(id),
    item_size_id        bigint references item_size(id),
    item_detail_id      bigint references item_detail(id),
    created_date        timestamp with time zone not null default now()
);
comment on table items is 'Таблица о товарах';
comment on column items.item_type_id is 'Тип к котору относится товар "Футболка"';
comment on column items.vendor_code is 'Артикул товара';
comment on column items.name is 'Название товара';
comment on column items.price is 'Цена товара';
comment on column items.product_evaluation is 'Оценка товара';
comment on column items.item_colors_id is 'Цвет товара';
comment on column items.item_size_id is 'Размеры товара';
comment on column items.item_detail_id is 'Детали товара';
comment on column items.created_date is 'Дата создания';